
import static org.junit.Assert.*;

import org.junit.Test;


class TestApu {

	@Test
	void test() {
		//assertEquals("APU SE QUEDA", ApuSeVa.apu(1,"apu"));
		assertEquals("0 APU SE VA DE LOS SIMPSON", ApuSeVa.apu(2,"AmApOLa"));
		assertEquals("3 APU SE QUEDA", ApuSeVa.apu(3,"AaPpUupUA"));
		assertEquals("1 APU SE QUEDA", ApuSeVa.apu(1,"UPA"));
		assertEquals("0 APU SE VA DE LOS SIMPSON", ApuSeVa.apu(1,"ePaEpA"));
		assertEquals("1 APU SE QUEDA", ApuSeVa.apu(1,"Sopa de Macaco UMA deliSia"));
		assertEquals("3 APU SE VA DE LOS SIMPSON", ApuSeVa.apu(5,"aPpUaApApaUu"));
		assertEquals("14 APU SE VA DE LOS SIMPSON", ApuSeVa.apu(20,"APODUAIOSDSIYYIpayiayiayyiYAYYAPIUAUAOUAIOUAIYAUuipauuyapuau aupoiayiuUAIUAPOUAPAUYupaayuuyiaipudbvuajhdbaidapiuuPIAUDPAUPI"));
		assertEquals("173 APU SE QUEDA", ApuSeVa.apu(100,"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaapppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu"));
		assertEquals("5 APU SE QUEDA", ApuSeVa.apu(4,"apÃºSAMUElÃ puertÃ¡aveSIOTonTOqUienLoLeÃ€saNOpAPÃ™ PESCTAUN+Ã€ ASÃ ltÃ�cuNÃ S"));
		assertEquals("0 APU SE VA DE LOS SIMPSON", ApuSeVa.apu(1,"PÃša"));
		assertEquals("1 APU SE VA DE LOS SIMPSON", ApuSeVa.apu(2,"Aplauso ante la inusual derrota del dream team"));
		assertEquals("2 APU SE QUEDA", ApuSeVa.apu(2,"PÃ tAtus Achu PaMeLA anderSOn Ã NEmIA "));
		assertEquals("2 APU SE VA DE LOS SIMPSON", ApuSeVa.apu(4,"NO SE ME OCurre nada MÃ¡S par Apu y El prObrlema este, fin"));
		
	}

}
