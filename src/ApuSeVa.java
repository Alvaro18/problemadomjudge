
import java.util.Scanner;
/**
 * @author      Alvaro barba <alvaro18cat@gmail.com>  (nom i correu)
 * @version     1.0                 (versió actual del programa)
 * @since       1.0          (La primera versió del programa
 */
public class ApuSeVa {

	public static void main(String[] args) {
		Scanner tecla = new Scanner(System.in);
		while(tecla.hasNext()) {
			int a=tecla.nextInt();
			String s=tecla.nextLine();
			System.out.println(apu(a, s));
		}
	}
	
	/**
     * A partir de la string trobara i el numero trobara si apu se va o se queda     (1)
     * <p>
     * Compara el que retorna la funcio contarapu(String s) amb el parametre Int a   (2)
     *
     * @param  int a Numero de vegades que te que sortir la paraula "apu"      (3)
     * @param  int s Frase que conte o no la paraula "apu"
     * @return Retorna un String (APU SE VA DE LOS SIMPSON / APU SE QUEDA) segons la comparacio
     */
	public static String apu(int a, String s) {
		if(contarapus(s)<a) {
			return contarapus(s) + " APU SE VA DE LOS SIMPSON";
		}else {
			return contarapus(s) + " APU SE QUEDA";
		}
	}

	/**
     * A partir de la string trobara el numero de "apu"s que contingui       (1)
     * <p>
     * Llegira paraula per paraula d'esquerra a dreta per trobar cuantes     (2)
     * vegades sur la paraula "apu"
     * <p>
     * Si voleu posar més explicació les podeu anar posant
     * Com veieu els tags html funcionen en javadoc
     *
     * @param  int a Numero de vegades que te que sortir la paraula "apu"    (3)
     * @param  int s Frase que conte o no la paraula "apu"
     * @return Retorna un String (APU SE VA DE LOS SIMPSON / APU SE QUEDA) segons la comparacio
     */
	private static int contarapu(String s) {
		int contador=0;
		int minicontador=0;
		for(int i=0; i<s.length();i++) {
			if((s.charAt(i)=='a' || s.charAt(i)=='A') && minicontador==0) {
				minicontador++;
			}else if((s.charAt(i)=='p' || s.charAt(i)=='P') && minicontador==1) {
				minicontador++;
			}else if((s.charAt(i)=='u' || s.charAt(i)=='U') && minicontador==2) {
				contador++;
				minicontador=0;
			}
		}
		return contador;
	}
	/**
     * A partir de la string trobara el numero de "apu"s que contingui       (1)
     * <p>
     * Llegira paraula per paraula d'esquerra a dreta per trobar cuantes     (2)
     * vegades sur la paraula "apu"
     * <p>
     * Si voleu posar més explicació les podeu anar posant
     * Com veieu els tags html funcionen en javadoc
     *
     * @param  int a Numero de vegades que te que sortir la paraula "apu"    (3)
     * @param  int s Frase que conte o no la paraula "apu"
     * @return Retorna un String (APU SE VA DE LOS SIMPSON / APU SE QUEDA) segons la comparacio
     */
	private static int contarapus(String s) {
		int contador=0;
		int minicontadora=0;
		int minicontadorp=0;
		int minicontadoru=0;
		for(int i=0; i<s.length();i++) {
			if((s.charAt(i)=='a' || s.charAt(i)=='A')) {
				minicontadora++;
			}else if((s.charAt(i)=='p' || s.charAt(i)=='P')) {
				minicontadorp++;
			}else if((s.charAt(i)=='u' || s.charAt(i)=='U')) {
				contador++;
				minicontadoru++;
			}
		}
		return Math.min(Math.min(minicontadora, minicontadorp), minicontadoru);
	}

}
